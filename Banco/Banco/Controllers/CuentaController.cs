﻿using Banco.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Banco.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    [Authorize]
    public class CuentaController : ControllerBase
    {
        private readonly ICuentaService _cuentaService;

        public CuentaController(ICuentaService cuentaService)
        {
            _cuentaService = cuentaService;
        }

        [HttpGet("ObtenerDatos")]
        public IActionResult ObtenerDatos(string numeroTarjeta)
        {
            var response = _cuentaService.ObtenerDatos(numeroTarjeta); 
            if (response.Exitoso)
            {
                return Ok(response);
            }

            return BadRequest(response.Mensaje);
        }

        [HttpGet("Extraccion")]
        public IActionResult Extraccion(string numeroTarjeta, decimal monto)
        {
            var response = _cuentaService.Extraccion(numeroTarjeta, monto);
            if (response.Exitoso)
            {
                return Ok(response);
            }

            return BadRequest(response.Mensaje);
        }

        [HttpGet("operaciones")]
        public IActionResult ObtenerOperaciones(string numeroTarjeta, int numeroPagina, int tamañoPagina)
        {
            var response = _cuentaService.ObtenerOperaciones(numeroTarjeta, numeroPagina, tamañoPagina);
            if (response.Exitoso)
            {
                return Ok(response.Data);
            }

            return BadRequest(response.Mensaje);
        }

    }
}
