﻿using Banco.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Banco.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class LoginController : ControllerBase
    {
        private readonly ILoginService _loginService;

        public LoginController(ILoginService authService)
        {
            _loginService = authService;
        }

        [HttpPost("Login")]
        public IActionResult Login(string numeroTarjeta, string pin)
        {
            var respuesta = _loginService.Login(numeroTarjeta, pin);

            if (respuesta.Exitoso == true)
            {
                return Ok(new { Token = respuesta.Data });
            }

            return BadRequest(respuesta.Mensaje);
        }
    }
}
