﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Banco.Utilidades
{
    public class ListaPaginada<T>
    {
        public int NumeroPagina { get; private set; }
        public int TotalPaginas { get; private set; }
        public int TamañoPagina { get; private set; }
        public List<T> Elementos { get; private set; }

        public ListaPaginada(List<T> elementos, int numeroTotalElementos, int numeroPagina, int tamañoPagina)
        {
            NumeroPagina = numeroPagina;
            TamañoPagina = tamañoPagina;
            TotalPaginas = (int)Math.Ceiling(numeroTotalElementos / (double)tamañoPagina);

            Elementos = elementos.Skip((numeroPagina - 1) * tamañoPagina)
                                .Take(tamañoPagina)
                                .ToList();
        }

        public static ListaPaginada<T> Crear(IQueryable<T> fuente, int numeroPagina, int tamañoPagina)
        {
            var totalElementos = fuente.Count();
            var elementos = fuente.ToList();
            return new ListaPaginada<T>(elementos, totalElementos, numeroPagina, tamañoPagina);
        }
    }
}
