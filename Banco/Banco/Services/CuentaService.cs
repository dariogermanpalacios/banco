﻿using Banco.Dtos;
using Banco.Models;
using Banco.Repositories.Interfaces;
using Banco.Services.Interfaces;
using Banco.Utilidades;
using Microsoft.Extensions.Logging;
using System;

namespace Banco.Services
{
    public class CuentaService : ICuentaService
    {
        private readonly ICuentaRepository _cuentaRepository;
        private readonly ILogger<CuentaService> _logger;

        public CuentaService(ICuentaRepository cuentaRepository, ILogger<CuentaService> logger)
        {
            _cuentaRepository = cuentaRepository;
            _logger = logger;
        }

        /// <summary>
        /// Obtiene datos de la cuenta dado un numero de tarjeta
        /// </summary>
        /// <param name="numeroTarjeta"></param>
        /// <returns>Datos de la cuenta</returns>
        public Respuesta<InformacionTarjetaDto> ObtenerDatos(string numeroTarjeta)
        {
            var respuesta = new Respuesta<InformacionTarjetaDto>();

            try
            {
                _logger.LogInformation("ObtenerDatos - Obteniendo datos de cuenta");
                var resultado = _cuentaRepository.ObtenerDatos(numeroTarjeta);
                if (resultado != null)
                {
                    respuesta.Data = resultado;
                    respuesta.Exitoso = true;
                    respuesta.Mensaje = "Exitoso.";
                }
                else
                {
                    respuesta.Exitoso = false;
                    respuesta.Mensaje = "No se encuentra la tarjeta.";
                }

            }
            catch (Exception ex)
            {
                _logger.LogInformation("ObtenerDatos - Ocurrio el siguiente error: {@ex}", ex);
                respuesta.Mensaje = ex.Message;
            }

            return respuesta;
        }

        /// <summary>
        /// Realiza una extraccion del monto dado un numero de tarjeta y monto
        /// </summary>
        /// <param name="numeroTarjeta"></param>
        /// <param name="monto"></param>
        /// <returns>Respuesta</returns>
        public Respuesta<string> Extraccion(string numeroTarjeta, decimal monto)
        {
            var respuesta = new Respuesta<string>();

            try
            {
                _logger.LogInformation("Extraccion - Realizando la extraccion");
                var resultado = _cuentaRepository.Extraccion(numeroTarjeta, monto);
                if (resultado)
                {
                    respuesta.Data = $"Se extrajo el monto: {monto} de esta tarjeta: {numeroTarjeta} exitosamente.";
                    respuesta.Exitoso = true;
                    respuesta.Mensaje = "Exitoso.";
                }
                else
                {
                    respuesta.Data = $"No hay suficiente saldo para realizar esta extraccion.";
                    respuesta.Exitoso = false;
                    respuesta.Mensaje = "Fallido.";
                }
            }
            catch (Exception ex)
            {
                _logger.LogInformation("Extraccion - Ocurrio el siguiente error: {@ex}", ex);
                respuesta.Mensaje = ex.Message;
            }

            return respuesta;
        }

        /// <summary>
        /// Devuelve
        /// </summary>
        /// <param name="numeroTarjeta"></param>
        /// <param name="numeroPagina"></param>
        /// <param name="tamañoPagina"></param>
        /// <returns></returns>
        public Respuesta<ListaPaginada<OperacionDto>> ObtenerOperaciones(string numeroTarjeta, int numeroPagina, int tamañoPagina)
        {
            var respuesta = new Respuesta<ListaPaginada<OperacionDto>>();

            try
            {
                _logger.LogInformation("ObtenerOperaciones - Obteniendo datos de operaciones");
                var resultado = _cuentaRepository.ObtenerOperaciones(numeroTarjeta, numeroPagina, tamañoPagina);

                respuesta.Data = resultado;
                respuesta.Exitoso = true;
                respuesta.Mensaje = "Operaciones obtenidas exitosamente.";
            }
            catch (Exception ex)
            {
                _logger.LogError("ObtenerOperaciones - Ocurrió el siguiente error: {MensajeError}", ex.Message);
                respuesta.Exitoso = false;
                respuesta.Mensaje = "Ocurrió un error al obtener las operaciones.";
            }

            return respuesta;
        }

    }
}
