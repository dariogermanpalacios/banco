﻿using Banco.Dtos;
using Banco.Models;
using Banco.Utilidades;
using System.Threading.Tasks;

namespace Banco.Services.Interfaces
{
    public interface ILoginService
    {
        Respuesta<string> Login(string numeroTarjeta, string pin);
    }
}
