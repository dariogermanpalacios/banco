﻿using Banco.Dtos;
using Banco.Models;
using Banco.Utilidades;
using System.Threading.Tasks;


namespace Banco.Services.Interfaces
{
    public interface ICuentaService
    {
        Respuesta<InformacionTarjetaDto> ObtenerDatos(string numeroTarjeta);
        Respuesta<string> Extraccion(string numeroTarjeta, decimal monto);
        Respuesta<ListaPaginada<OperacionDto>> ObtenerOperaciones(string numeroTarjeta, int numeroPagina, int tamañoPagina);
    }
}
