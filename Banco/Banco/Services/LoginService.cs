﻿using Banco.Models;
using Banco.Repositories.Interfaces;
using Banco.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Serilog.Core;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Banco.Services
{
    public class LoginService : ILoginService
    {
        private readonly IConfiguration _configuration;
        private readonly ILoginRepository _loginRepository;
        private readonly ILogger<LoginService> _logger;

        public LoginService(IConfiguration configuration, ILoginRepository loginRepository, ILogger<LoginService> logger)
        {
            _configuration = configuration;
            _loginRepository = loginRepository;
            _logger = logger;
        }

        public Respuesta<string> Login(string numeroTarjeta, string pin)
        {
            var respuesta = new Respuesta<string>();

            try
            {
                var tarjeta = _loginRepository.ObtenerTarjetaPorNumero(numeroTarjeta);

                if (tarjeta == null || tarjeta.Bloqueada == true)
                {
                    respuesta.Data = "";
                    respuesta.Exitoso = false;
                    respuesta.Mensaje = "La tarjeta está bloqueada o no existe.";
                }
                else if (tarjeta.Pin != pin)
                {
                    IncrementarIntentosFallidos(tarjeta);
                    respuesta.Data = "";
                    respuesta.Exitoso = false;
                    respuesta.Mensaje = "PIN incorrecto.";
                }
                else
                {

                    ResetearIntentosFallidos(tarjeta);
                    var token = GenerarTokenJwt(numeroTarjeta);
                    respuesta.Data = token;
                    respuesta.Exitoso = true;
                    respuesta.Mensaje = "Exitoso.";
                }
            }
            catch (Exception ex)
            {
                _logger.LogError("ObtenerOperaciones - Ocurrió el siguiente error: {MensajeError}", ex.Message);
                respuesta.Exitoso = false;
                respuesta.Mensaje = "Ocurrió un error al obtener las operaciones.";
            }

            return respuesta;
        }

        private void IncrementarIntentosFallidos(Tarjeta tarjeta)
        {
            tarjeta.IntentosFallidos++;

            if (tarjeta.IntentosFallidos >= 4)
            {
                tarjeta.Bloqueada = true;
                tarjeta.FechaBloqueo = DateTime.Now;
            }

            _loginRepository.ActualizarTarjeta(tarjeta);
        }

        private void ResetearIntentosFallidos(Tarjeta tarjeta)
        {
            tarjeta.IntentosFallidos = 0;
            _loginRepository.ActualizarTarjeta(tarjeta);
        }


        private string GenerarTokenJwt(string numeroTarjeta)
        {
            var secretKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["Jwt:SecretKey"]));
            var signingCredentials = new SigningCredentials(secretKey, SecurityAlgorithms.HmacSha256);

            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, numeroTarjeta),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            var tokenOptions = new JwtSecurityToken(
                issuer: _configuration["Jwt:Issuer"],
                audience: _configuration["Jwt:Audience"],
                claims: claims,
                expires: DateTime.UtcNow.AddMinutes(Convert.ToDouble(_configuration["Jwt:ExpirationInMinutes"])),
                signingCredentials: signingCredentials
            );

            var tokenString = new JwtSecurityTokenHandler().WriteToken(tokenOptions);

            return tokenString;
        }
    }
}
