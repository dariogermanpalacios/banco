﻿using Banco.Dtos;
using Banco.Models;
using Banco.Repositories.Interfaces;
using Banco.Utilidades;
using System;
using System.Linq;

namespace Banco.Repositories
{
    public class CuentaRepository : ICuentaRepository
    {
        private readonly BancoContext _contexto;

        public CuentaRepository(BancoContext contexto)
        {
            _contexto = contexto;
        }

        public InformacionTarjetaDto ObtenerDatos(string numeroTarjeta)
        {
            var datos = _contexto.Tarjeta
                .Where(x => x.NumeroTarjeta == numeroTarjeta)
                .Select(tarjeta => new InformacionTarjetaDto()
                {
                    FechaUltimaExtraccion = tarjeta.FechaUltimaExtraccion,
                    NombreUsuario = tarjeta.IdUsuarioNavigation.Nombre,
                    NumeroCuenta = tarjeta.IdUsuarioNavigation.Id,
                    SaldoActual = tarjeta.Saldo
                })
                .FirstOrDefault();

            return datos;
        }

        public bool Extraccion(string numeroTarjeta, decimal monto)
        {
            var tarjeta = _contexto.Tarjeta.FirstOrDefault(x => x.NumeroTarjeta == numeroTarjeta);

            if (tarjeta.Saldo < monto)
            {
                return false;
            }

            tarjeta.Saldo -= monto;
            tarjeta.FechaUltimaExtraccion = DateTime.Now;

            _contexto.SaveChanges();

            /*     AGREGAR OPERACION     */

            var operacion = new Operacion
            {
                IdTarjeta = tarjeta.Id,
                Tipo = "Extracción",
                Monto = monto,
                Fecha = DateTime.Now
            };

            _contexto.Operacion.Add(operacion);
            _contexto.SaveChanges();

            return true;
        }

        public ListaPaginada<OperacionDto> ObtenerOperaciones(string numeroTarjeta, int numeroPagina, int tamañoPagina)
        {
            var consultaOperaciones = _contexto.Operacion
                .Where(op => op.IdTarjetaNavigation.NumeroTarjeta == numeroTarjeta)
                .OrderByDescending(op => op.Fecha); // Ordena las operaciones por fecha descendente

            var operacionesPaginadas = ListaPaginada<OperacionDto>.Crear(consultaOperaciones
                .Select(op => new OperacionDto
                {
                    Tipo = op.Tipo,
                    Monto = op.Monto,
                    Fecha = op.Fecha
                }), numeroPagina, tamañoPagina);

            return operacionesPaginadas;
        }
    }
}
