﻿using Banco.Models;
using Banco.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Banco.Repositories
{
    public class LoginRepository : ILoginRepository
    {
        private readonly BancoContext _contexto;

        public LoginRepository(BancoContext contexto)
        {
            _contexto = contexto;
        }

        public Usuario ObtenerUsuarioPorTarjeta(string numeroTarjeta)
        {
            var usuario = _contexto.Usuario
                .Include(u => u.Tarjeta)
                .FirstOrDefault(u => u.Tarjeta.Any(t => t.NumeroTarjeta == numeroTarjeta));

            return usuario;
        }

        public Tarjeta ObtenerTarjetaPorNumero(string numeroTarjeta)
        {
            return _contexto.Tarjeta.FirstOrDefault(t => t.NumeroTarjeta == numeroTarjeta);
        }

        public void ActualizarTarjeta(Tarjeta tarjeta)
        {
            _contexto.Update(tarjeta);
            _contexto.SaveChanges();
        }
    }
}
