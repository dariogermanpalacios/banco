﻿using Banco.Dtos;
using Banco.Utilidades;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Banco.Repositories.Interfaces
{
    public interface ICuentaRepository
    {
        InformacionTarjetaDto ObtenerDatos(string numeroTarjeta);
        bool Extraccion(string numeroTarjeta, decimal monto);
        ListaPaginada<OperacionDto> ObtenerOperaciones(string numeroTarjeta, int numeroPagina, int tamañoPagina);
    }
}
