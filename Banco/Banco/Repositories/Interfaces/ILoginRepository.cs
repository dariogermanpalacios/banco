﻿using Banco.Dtos;
using Banco.Models;
using Banco.Utilidades;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Banco.Repositories.Interfaces
{
    public interface ILoginRepository
    {
        Usuario ObtenerUsuarioPorTarjeta(string numeroTarjeta);
        Tarjeta ObtenerTarjetaPorNumero(string numeroTarjeta);
        void ActualizarTarjeta(Tarjeta tarjeta);
    }
}
