﻿using System;

namespace Banco.Dtos
{
    public class InformacionTarjetaDto
    {
        public string NombreUsuario { get; set; }
        public int NumeroCuenta { get; set; }
        public decimal SaldoActual { get; set; }
        public DateTime? FechaUltimaExtraccion { get; set; }

    }
}
