﻿using System;

namespace Banco.Dtos
{
    public class OperacionDto
    {
        public string Tipo { get; set; }
        public decimal? Monto { get; set; }
        public DateTime? Fecha { get; set; }

    }
}
