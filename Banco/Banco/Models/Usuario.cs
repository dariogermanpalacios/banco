﻿using System;
using System.Collections.Generic;

namespace Banco.Models
{
    public partial class Usuario
    {
        public Usuario()
        {
            Tarjeta = new HashSet<Tarjeta>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public virtual ICollection<Tarjeta> Tarjeta { get; set; }
    }
}
