﻿using System;
using System.Collections.Generic;

namespace Banco.Models
{
    public partial class Tarjeta
    {
        public Tarjeta()
        {
            Operacions = new HashSet<Operacion>();
        }

        public int Id { get; set; }
        public int? IdUsuario { get; set; }
        public string NumeroTarjeta { get; set; }
        public string Pin { get; set; }
        public decimal Saldo { get; set; }
        public bool? Bloqueada { get; set; }
        public DateTime? FechaBloqueo { get; set; }
        public DateTime? FechaUltimaExtraccion { get; set; }
        public int IntentosFallidos { get; set; }
        public virtual Usuario IdUsuarioNavigation { get; set; }
        public virtual ICollection<Operacion> Operacions { get; set; }
    }
}
