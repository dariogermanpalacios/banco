﻿using System;
using System.Collections.Generic;

namespace Banco.Models
{
    public partial class Operacion
    {
        public int Id { get; set; }
        public int? IdTarjeta { get; set; }
        public string Tipo { get; set; }
        public decimal? Monto { get; set; }
        public DateTime? Fecha { get; set; }

        public virtual Tarjeta IdTarjetaNavigation { get; set; }
    }
}
