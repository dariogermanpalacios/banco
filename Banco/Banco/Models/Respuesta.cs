﻿namespace Banco.Models
{
    public class Respuesta<T>
    {
        public T Data { get; set; }
        public bool Exitoso { get; set; }
        public string Mensaje { get; set; }
    }
}
