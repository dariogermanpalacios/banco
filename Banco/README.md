# Banco

# Proyecto

Este proyecto simula el procesamiento de informacion de un banco, en el que se puede:


  * Iniciar sesion
  * Consultas
  * Extracciones
  * Listas de resultados

# Notas

  * URL: https://localhost:44385/swagger/index.html
  * Base de datos en script: SQLBanco.sql

#Relaciones

  * Un usuario puede tener varias tarjetas, pero una tarjeta pertenece a un solo usuario. (Relación Uno a Muchos entre Usuario y Tarjeta)
  * Una tarjeta puede tener varias operaciones, pero una operación pertenece a una sola tarjeta. (Relación Uno a Muchos entre Tarjeta y Operación)
