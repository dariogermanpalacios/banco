﻿using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using Banco.Services.Interfaces;
using Banco.Controllers;
using Banco.Models;
using Banco.Dtos;
using Banco.Utilidades;

namespace Banco_Test.Repositories
{
    public class CuentaControllerTest
    {
        CuentaController target;
        Mock<ICuentaService> _mockCuentaService;
        public CuentaControllerTest()
        {
            _mockCuentaService = new Mock<ICuentaService>();
            target = new CuentaController(_mockCuentaService.Object);
        }

        [Test]
        public void ObtenerDatos_DeberiaRetornarBadRequestStatusCode_Cuando_ExitosoEsFalso()
        {
            //Arrange
            var numeroTarjeta = "123";
            var respuesta = new Respuesta<InformacionTarjetaDto>();
            respuesta.Exitoso = false;

            _mockCuentaService
                .Setup(m => m.ObtenerDatos(numeroTarjeta)).Returns(respuesta);

            //Act
            var res = target.ObtenerDatos(numeroTarjeta);
            var badRequestResultado = res as BadRequestObjectResult;

            //Assert
            Assert.IsNotNull(res);
            Assert.AreEqual(400, badRequestResultado.StatusCode);
        }

        [Test]
        public void Extraccion_DeberiaRetornarOkStatusCode_Cuando_ExitosoEsVerdadero()
        {
            // Arrange
            var numeroTarjeta = "123";
            var monto = 100;
            var respuesta = new Respuesta<string> { Exitoso = true };

            _mockCuentaService
                .Setup(m => m.Extraccion(numeroTarjeta, monto)).Returns(respuesta);

            // Act
            var res = target.Extraccion(numeroTarjeta, monto);
            var okResultado = res as OkObjectResult;

            // Assert
            Assert.IsNotNull(res);
            Assert.AreEqual(200, okResultado.StatusCode);
        }

        [Test]
        public void Extraccion_DeberiaRetornarBadRequestStatusCode_Cuando_ExitosoEsFalso()
        {
            // Arrange
            var numeroTarjeta = "123";
            var monto = 100;
            var respuesta = new Respuesta<string> { Exitoso = false };

            _mockCuentaService
                .Setup(m => m.Extraccion(numeroTarjeta, monto)).Returns(respuesta);

            // Act
            var res = target.Extraccion(numeroTarjeta, monto);
            var badRequestResultado = res as BadRequestObjectResult;

            // Assert
            Assert.IsNotNull(res);
            Assert.AreEqual(400, badRequestResultado.StatusCode);
        }

        [Test]
        public void ObtenerOperaciones_DeberiaRetornarOkStatusCode_Cuando_ExitosoEsVerdadero()
        {
            // Arrange
            var numeroTarjeta = "123";
            var numeroPagina = 1;
            var tamañoPagina = 10;
            var respuesta = new Respuesta<ListaPaginada<OperacionDto>> { Exitoso = true };

            _mockCuentaService
                .Setup(m => m.ObtenerOperaciones(numeroTarjeta, numeroPagina, tamañoPagina)).Returns(respuesta);

            // Act
            var res = target.ObtenerOperaciones(numeroTarjeta, numeroPagina, tamañoPagina);
            var okResultado = res as OkObjectResult;

            // Assert
            Assert.IsNotNull(res);
            Assert.AreEqual(200, okResultado.StatusCode);
        }

        [Test]
        public void ObtenerOperaciones_DeberiaRetornarBadRequestStatusCode_Cuando_ExitosoEsFalso()
        {
            // Arrange
            var numeroTarjeta = "123";
            var numeroPagina = 1;
            var tamañoPagina = 10;
            var respuesta = new Respuesta<ListaPaginada<OperacionDto>> { Exitoso = false };

            _mockCuentaService
                .Setup(m => m.ObtenerOperaciones(numeroTarjeta, numeroPagina, tamañoPagina)).Returns(respuesta);

            // Act
            var res = target.ObtenerOperaciones(numeroTarjeta, numeroPagina, tamañoPagina);
            var badRequestResultado = res as BadRequestObjectResult;

            // Assert
            Assert.IsNotNull(res);
            Assert.AreEqual(400, badRequestResultado.StatusCode);
        }


    }
}
