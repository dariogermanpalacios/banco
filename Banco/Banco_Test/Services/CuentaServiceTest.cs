﻿using Banco.Dtos;
using Banco.Models;
using Banco.Repositories.Interfaces;
using Banco.Services;
using Banco.Services.Interfaces;
using Banco.Utilidades;
using Microsoft.Extensions.Logging;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Banco_Test.Services
{
    public class CuentaServiceTest
    {
        private ICuentaService _cuentaService;
        private Mock<ICuentaRepository> _mockCuentaRepository;
        private Mock<ILogger<CuentaService>> _mockLogger;

        [SetUp]
        public void Setup()
        {
            _mockCuentaRepository = new Mock<ICuentaRepository>();
            _mockLogger = new Mock<ILogger<CuentaService>>();
            _cuentaService = new CuentaService(_mockCuentaRepository.Object, _mockLogger.Object);
        }

        [Test]
        public void ObtenerDatos_DeberiaRetornarRespuestaExitosa_CuandoLaTarjetaExiste()
        {
            // Arrange
            var numeroTarjeta = "123";
            var informacionTarjeta = new InformacionTarjetaDto();
            _mockCuentaRepository.Setup(repo => repo.ObtenerDatos(numeroTarjeta)).Returns(informacionTarjeta);

            // Act
            var respuesta = _cuentaService.ObtenerDatos(numeroTarjeta);

            // Assert
            Assert.IsTrue(respuesta.Exitoso);
            Assert.AreEqual("Exitoso.", respuesta.Mensaje);
            Assert.AreEqual(informacionTarjeta, respuesta.Data);
        }

        [Test]
        public void ObtenerDatos_DeberiaRetornarRespuestaFallida_CuandoLaTarjetaNoExiste()
        {
            // Arrange
            var numeroTarjeta = "123";
            _mockCuentaRepository.Setup(repo => repo.ObtenerDatos(numeroTarjeta)).Returns((InformacionTarjetaDto)null);

            // Act
            var respuesta = _cuentaService.ObtenerDatos(numeroTarjeta);

            // Assert
            Assert.IsFalse(respuesta.Exitoso);
            Assert.AreEqual("No se encuentra la tarjeta.", respuesta.Mensaje);
            Assert.IsNull(respuesta.Data);
        }

        [Test]
        public void Extraccion_DeberiaRetornarRespuestaExitosa_CuandoLaExtraccionEsExitosa()
        {
            // Arrange
            var numeroTarjeta = "123";
            var monto = 100M;
            _mockCuentaRepository.Setup(repo => repo.Extraccion(numeroTarjeta, monto)).Returns(true);

            // Act
            var respuesta = _cuentaService.Extraccion(numeroTarjeta, monto);

            // Assert
            Assert.IsTrue(respuesta.Exitoso);
            Assert.AreEqual("Exitoso.", respuesta.Mensaje);
            Assert.IsNotNull(respuesta.Data);
            Assert.AreEqual($"Se extrajo el monto: {monto} de esta tarjeta: {numeroTarjeta} exitosamente.", respuesta.Data);
        }

        [Test]
        public void Extraccion_DeberiaRetornarRespuestaFallida_CuandoNoHaySuficienteSaldo()
        {
            // Arrange
            var numeroTarjeta = "123";
            var monto = 100M;
            _mockCuentaRepository.Setup(repo => repo.Extraccion(numeroTarjeta, monto)).Returns(false);

            // Act
            var respuesta = _cuentaService.Extraccion(numeroTarjeta, monto);

            // Assert
            Assert.IsFalse(respuesta.Exitoso);
            Assert.AreEqual("Fallido.", respuesta.Mensaje);
            Assert.IsNotNull(respuesta.Data);
            Assert.AreEqual("No hay suficiente saldo para realizar esta extraccion.", respuesta.Data);
        }

        [Test]
        public void ObtenerOperaciones_DeberiaRetornarRespuestaExitosa_CuandoLaOperacionEsExitosa()
        {
            // Arrange
            var numeroTarjeta = "123";
            var numeroPagina = 1;
            var tamañoPagina = 10;
            var operacion = new List<OperacionDto>();
            var operaciones = new ListaPaginada<OperacionDto>(operacion, 10, numeroPagina, tamañoPagina);
            _mockCuentaRepository.Setup(repo => repo.ObtenerOperaciones(numeroTarjeta, numeroPagina, tamañoPagina)).Returns(operaciones);

            // Act
            var respuesta = _cuentaService.ObtenerOperaciones(numeroTarjeta, numeroPagina, tamañoPagina);

            // Assert
            Assert.IsTrue(respuesta.Exitoso);
            Assert.AreEqual("Operaciones obtenidas exitosamente.", respuesta.Mensaje);
            Assert.AreEqual(operaciones, respuesta.Data);
        }

        [Test]
        public void ObtenerOperaciones_DeberiaRetornarRespuestaFallida_CuandoOcurreUnError()
        {
            // Arrange
            var numeroTarjeta = "123";
            var numeroPagina = 1;
            var tamañoPagina = 10;
            _mockCuentaRepository.Setup(repo => repo.ObtenerOperaciones(numeroTarjeta, numeroPagina, tamañoPagina)).Throws(new Exception("Error de prueba"));

            // Act
            var respuesta = _cuentaService.ObtenerOperaciones(numeroTarjeta, numeroPagina, tamañoPagina);

            // Assert
            Assert.IsFalse(respuesta.Exitoso);
            Assert.AreEqual("Ocurrió un error al obtener las operaciones.", respuesta.Mensaje);
            Assert.IsNull(respuesta.Data);
        }
    }
}
